<?php

namespace Database\Seeders;

use App\Models\Blog;
use App\Models\Contact;
use Illuminate\Database\Seeder;
use App\Models\Curso;
use App\Models\User;
use App\Models\Portfolio;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // llamar seeder curso
        //$this->call(CursoSeeder::class);

        // llamar directamente el factory solo se importa el modelo
        User::factory(10)->create();
        Curso::factory(50)->create();
        Portfolio::factory(50)->create();
        Blog::factory(50)->create();
        Contact::factory(50)->create();
    }
}
