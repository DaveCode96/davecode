<?php

namespace Database\Seeders;

use App\Models\Curso;
use Illuminate\Database\Seeder;

class CursoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // --------------------ejecutar un seeder registros manuales------------------
        // $curso = new Curso();
     
        // $curso -> name = ('Laravel');
        // $curso -> description = ('Es un framework de php');
        // $curso -> category = ('Programacion');
      
        // $curso->save();

        // $curso2 = new Curso();
     
        // $curso2 -> name = ('Laravel');
        // $curso2 -> description = ('Es un framework de php');
        // $curso2 -> category = ('Programacion');
      
        // $curso2->save();

        // $curso3 = new Curso();
     
        // $curso3 -> name = ('Laravel');
        // $curso3 -> description = ('Es un framework de php');
        // $curso3 -> category = ('Programacion');
      
        // $curso3->save();


        // -------------------Ejecutar un factory desde el seeder-------------

        Curso::factory(50)->create();
    }
}
