<?php

namespace App\Http\Controllers;

use App\Models\Curso;
use Illuminate\Http\Request;

class CursoController extends Controller
{
    public function index(){

        $cursos = Curso::paginate();

        // nos regresa toda una coleccion de los regristros
        // return $cursos;

        return view('Cursos.index', compact('cursos'));
    }

    public function create(){
        return view('Cursos.create');
    }

    public function show($curso){
        return view('Cursos.show', compact('curso'));
        //['curso' => $curso] si las variables no coinciden
    }
}
