<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PortfolioController extends Controller
{
    public function index(){
        return "Bienvenido al inicio de portafolio";
    }
    public function create(){
        return "Aqui podemos crear un proyecto de portafolio nuevo";
    }
    public function show($project){
        return "El proyecto es: $project";
    }
}
