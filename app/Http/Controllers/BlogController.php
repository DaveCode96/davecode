<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use Illuminate\Http\Request;

class BlogController extends Controller
{

    public function index(){

        // $blogs = Blog::all()->paginate(8); necesito un where para que funcione en lugar del all
        $blogs = Blog::latest('id')->paginate(6);
        return view('Blogs.index', compact('blogs'));
    }


    public function show($article){
        return view('Blogs.show', compact('article'));
    }
}
