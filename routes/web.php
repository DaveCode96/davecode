<?php

use App\Http\Controllers\CursoController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AboutMeController;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\PortfolioController;
use App\Http\Controllers\ResumeController;
use App\Models\Resume;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// ---------------------------Inicio-------------------------------------
Route::get('/', HomeController::class);

// ---------------------------About me-------------------------------------
Route::get('AboutMe', [AboutMeController::class, 'index']);

// ---------------------------Resume-------------------------------------
Route::get('Resume', [ResumeController::class, 'index']);

// ---------------------------Portfolio-------------------------------------
// Route::get('Portfolio/', [PortfolioController::class, 'index']);

// Route::get('Portfolio/create', [PortfolioController::class, 'create']);

// Route::get('Portfolio/{project}', [PortfolioController::class, 'show']);

// ---------------------------Blog-------------------------------------

Route::get('Blogs', [BlogController::class, 'index']);
Route::get('Blogs/{article}', [BlogController::class, 'show']);

// ---------------------------Contact-------------------------------------
Route::get('Contacts', [ContactController::class, 'index']);

// ---------------------------Cursos-------------------------------------
Route::get('Cursos', [CursoController::class, 'index']);

Route::get('Cursos/create', [CursoController::class, 'create']);

Route::get('Cursos/{curso}', [CursoController::class, 'show']);

// Route::get('Cursos/{curso}/{categoria?}', function ($curso, $categoria=null) {
//     if($categoria){
//         return "Bienvenido al $curso, de la categoria: $categoria";
//     }else{
//         return "Bienvenido al curso: $curso";
//     }
// });
