<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>

    	<!-- CSS Plugins -->
        <link rel="stylesheet" href="css/plugins/bootstrap.min.css">
        <link rel="stylesheet" href="css/plugins/font-awesome.css">
		<link rel="stylesheet" href="css/plugins/magnific-popup.css">
		<link rel="stylesheet" href="css/plugins/simplebar.css">
		<link rel="stylesheet" href="css/plugins/owl.carousel.min.css">
		<link rel="stylesheet" href="css/plugins/owl.theme.default.min.css">
		<link rel="stylesheet" href="css/plugins/jquery.animatedheadline.css">
		<!-- CSS Base -->
        <link rel="stylesheet" class="back-color" href="css/style-dark.css">
</head>
<body>
    @yield('content')
    		<!-- Preloader -->
		<div id="preloader">
            <div class="loading-area">
              <div class="circle"></div>
            </div>
            <div class="left-side"></div>
            <div class="right-side"></div>
      </div>
      
      <!-- Main Site -->

          
          <div class="header-mobile">
              <a class="header-toggle"><i class="fas fa-bars"></i></a>
              <h2>Dave Code</h2>
          </div>
          
          <!-- Left Block -->
          <nav class="header-main" data-simplebar>
      
              <!-- Logo -->
              <div class="logo">
                  <img src="img/logo2.png" alt="">
              </div>
              
                <ul>
                  <li data-tooltip="Inicio" data-position="top">
                      <a href="/DaveCode/public/" class="icon-h fas fa-house-damage"></a>
                  </li>
                  <li data-tooltip="acerca de" data-position="top">
                      <a href="/DaveCode/public/AboutMe" class="icon-a fas fa-user-tie"></a>
                  </li>
                  <li data-tooltip="Currículum" data-position="top">
                      <a href="/DaveCode/public/Resume" class="icon-r fas fa-address-book"></a>
                  </li>
                  <li data-tooltip="portafolio" data-position="top">
                      <a href="#portfolio" class="icon-p fas fa-briefcase"></a>
                  </li>
                  <li data-tooltip="blog" data-position="top">
                      <a href="/DaveCode/public/Blogs" class="icon-b fas fa-receipt"></a>
                  </li>
                  <li data-tooltip="contacto" data-position="bottom">
                      <a href="/DaveCode/public/Contacts" class="icon-c fas fa-envelope"></a>
                  </li>
                </ul>
              
              {{-- <a class="music-bg">
                    <img src="img/logo2.png">
              </a> --}}
           </nav>

           



 
  
  <!-- All Script -->
  <script src="js/jquery.min.js"></script>
  <script src="js/isotope.pkgd.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/simplebar.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/jquery.animatedheadline.min.js"></script>
  <script src="js/jquery.easypiechart.js"></script>
  <script src="js/jquery.validation.js"></script>
  <script src="js/tilt.js"></script>
  <script src="js/main.js"></script>
  <script src="js/main-demo.js"></script>
  <script src="https://maps.google.com/maps/api/js?sensor=false"></script>
</body>
</html>