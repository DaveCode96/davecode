@extends ('layouts.plantilla')

@section('title', 'Contact')
    

@section('content')


<div class="pt-home" style="background-image: url('img/home.jpg')">
    <section>
    </section>
</div>

<!-- Contact Section -->
<div class="page pt-contact" data-simplebar>
    <section class="container">
        
        <!-- Section Title -->
          <div class="header-page mt-70 mob-mt">
            <h2>Contacto</h2>
            <span></span>
        </div>
        
        <!-- Form Start -->
        <div class="row mt-100">
            <div class="col-lg-12 col-sm-12">
                <div class="contact-form ">
                    <form method="post" class="box contact-valid" id="contact-form">
                        <div class="row">
                            <div class="col-lg-6 col-sm-12">
                                <input type="text" name="name" id="name" class="form-control" placeholder="Nombre *">
                            </div>
                            <div class="col-lg-6 col-sm-12">
                                <input type="email" name="email" id="email" class="form-control" placeholder="Correo *">
                            </div>
                            <div class="col-lg-12 col-sm-12">
                                <textarea class="form-control" name="note"  id="note" placeholder="Tu mensaje"></textarea>
                            </div>
                             <div class="col-lg-12 col-sm-12 text-center">
                                <button type="submit" class="btn-st">Enviar Mensaje</button>
                                <div id="loader">
                                    <i class="fas fa-sync"></i>
                                </div>
                            </div>
                            <div class="col-lg-12 col-sm-12">
                                <div class="error-messages">
                                    <div id="success">
                                        <i class="far fa-check-circle"></i>Gracias, tu mensaje a sido enviado.
                                    </div>
                                    <div id="error">
                                        <i class="far fa-times-circle"></i>Error al enviar tu mensaje. porfavor intentalo mas tarde.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        
        <!-- Contact Info -->
        <div class="box contact-info">
            <div class="row">
                <div class="col-lg-4 col-sm-12 info">
                    <i class="fas fa-paper-plane"></i>
                     <p>davecode96@gmail.com</p>
                      <span>Correo</span>
                </div>
                <div class="col-lg-4 col-sm-12 info">
                    <i class="fas fa-map-marker-alt"></i>
                     <p>Apizaco, Tlaxcala</p>
                      <span>Dirección</span>
                </div>	
                <div class="col-lg-4 col-sm-12 info">
                    <i class="fas fa-phone"></i>
                     <p>241-414-16-26</p>
                      <span>Telefono</span>
                </div>	
            </div>
        </div>


            
</section>

</div>


@endsection