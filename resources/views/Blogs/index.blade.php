@extends ('layouts.plantilla')

@section('title', 'Blog')
    

@section('content')

<div class="pt-home" style="background-image: url('img/home.jpg')">
    <section>
    </section>
</div>

   <!-- Blog Section -->
   <div class="page pt-blog" data-simplebar >
    <section class="container">
        
        <!-- Section Title -->
         <div class="header-page mt-70 mob-mt">
            <h2>Blog</h2>
            <span></span>
        </div>
        
        <!-- Blog Row Start -->

        
        <div class="row blog-masonry mt-100 mb-50">
            
                @foreach ($blogs as $blog)
                    	
                
                
            
            <!-- Blog Item -->
            <div class="col-lg-4 col-sm-6">
                <div class="blog-item">
                    <div class="thumbnail">
                        <a href="Blogs/{{$blog->name}}"><img alt="" src="img/blog/{{$blog->url_image}}"></a>
                        {{-- <a href="Blogs/{{$blog->name}}"><img alt="" src="Blogs{{Storage::url($blog->url_image->url)}}"></a> --}}
                    </div>
                    <h4><a href="Blogs/{{$blog->name}}">{{$blog->name}}</a></h4>
                    <ul>
                        <li><a href="#">{{$blog->created_at}}</a></li>
                        <li><a href="#">tema</a></li>
                    </ul>
                    <p>{{$blog->description}}..</p>
                    <div class="blog-btn">
                        <a href="Blogs/{{$blog->name}}" class="btn-st">Leer más</a>
                    </div>
                </div>
            </div>            
            @endforeach
        </div>
        
        <div class="pagination-lg pagination justify-content-center">
            {{ $blogs->links() }}
        </div>
   
          
    </section>
    
</div>


@endsection
