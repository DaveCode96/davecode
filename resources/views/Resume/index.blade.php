@extends ('layouts.plantilla')

@section('title', 'Resume')
    

@section('content')
<div class="pt-home" style="background-image: url('img/home.jpg')">
    <section>

          <!-- Resume Section -->
     <div class="page pt-resume" data-simplebar>
        <section class="container">
            
            <!-- Section Title -->
            <div class="header-page mt-70 mob-mt">
                <h2>Resume</h2>
                <span></span>
            </div>
            
            <!-- Experience & Education Row Start -->
            <div class="row mt-100">
            
                <!-- Experience Column Start -->
                <div class="col-lg-6 col-sm-12">
                
                    <!-- Header Block -->
                    <div class="header-box mb-50">
                        <h3>Experiencia</h3>
                    </div>
                
                    <div class="experience box">
                        
                        <!-- Experience Item -->
                        <div class="item">
                            <div class="main">
                                <h4>Pasante Desarrollador Web</h4>
                                <p><i class="far fa-calendar-alt"></i>Enero – Abril 2020 | SIB Ingeniería y Automatización S.A de C.V (Querétaro, MEX)</p>
                            </div>
                            <p>Desarrollo de aplicación web para reportes,control y automatización de inventario
                             (Python, Django, JavaScript,
                             Jquery, Sql server).</p>
                             <p>Implementacion de seguridad back-end y frond-end
                             para formularios con una vulnerabilidad en una conexión lenta (Ajax, JavaScript).</p>
                             
                        </div>
 
                        <!-- Experience Item -->
                        <div class="item">
                            <div class="main">
                                <h4>Pasante Desarrollador Web</h4>
                                <p><i class="far fa-calendar-alt"></i>Septiembre – Diciembre 2019 | SEDECO Tlaxcala (Tlaxcala, MEX)</p>
                            </div>
                            <p>Desarrollo de sitios web Ecommerce para microempresas implementando control de inventario y administracion de usuarios (Php,Ajax,Js,Mysql)</p>   
                            <p>Desarrollar el proyecto conjunto los emprendedores interesados, analizando sus ideas y necesidades para asi implementar un sitio web.</p>
                             
                        </div>
 
                        <!-- Experience Item -->
                        <div class="item">
                            <div class="main">
                                <h4>Pasante en Sistemas y
                                 Telecomunicaciones</h4>
                                <p><i class="far fa-calendar-alt"></i>Septiembre 2018-Abril 2019 | Lluvia S.A C.V group Vizcarra (CDMX, MEX) </p>
                            </div>
                            <p>Solucionar problemas que presente la empresa mediante instalación, mantenimiento preventivo, correctivo de camaras de seguridad y puntos de venta.</p>
                            <p>Administración de sitio web Ecommerce de la empresa, solucionar e implementar mejoras en el sitio web(Java,Servlet,jsp)</p>
                         </div>
                    </div>
                </div>
                    
                <!-- Education Column Start -->
                <div class="col-lg-6 col-sm-12">
                
                    <!-- Header Block -->
                    <div class="header-box mb-50 mob-box-mt">
                        <h3>Educación</h3>
                    </div>
                
                    <div class="experience box">
 
                        <!-- Education Item -->
                        <div class="item">
                            <div class="main">
                                <h4>Ingeniería en Tecnologías de la Información </h4>
                                <p><i class="far fa-calendar-alt"></i>2016 - Octubre 2020 | Universidad politécnica de Tlaxcala</p>
                            </div>
                            <p>Una educación es importante pero la perseverancia y la dedicación lo es incluso más.</p>
                        </div>
                        
                                               <!-- Education Item -->
                       <div class="item">
                             <div class="main">
                                     <h4>Desarrollo web Agenda escolar</h4>
                                        <p><i class="far fa-calendar-alt"></i> Diciembre 2019 | Universidad politécnica de Tlaxcala</p>
                             </div>
                                        <p>Desarrollo de sitio web para la universidad basado en la necesidad de una agenda escolar donde se muestren los eventos y los usuarios tengan roles para administrar su propios eventos dirigidos a la universidad(Php,Ajax,js,fullcalendar).</p>
                         </div>
 
                        <!-- Education Item -->
                        <div class="item">
                            <div class="main">
                                <h4>Online Digital Marketing Diploma</h4>
                                <p><i class="far fa-calendar-alt"></i> Noviembre 2018 | Universidad de Sevilla</p>
                            </div>
                            <p>Una buena combinación entre el desarrollo de un sitio web y una buena estrategia en los medios digitales es muy importante para tener un mayor alcance.</p>
                        </div>
 
                         <!-- Education Item -->
                         <div class="item">
                             <div class="main">
                                 <h4>Curso Cisco</h4>
                                 <p><i class="far fa-calendar-alt"></i> 2018-2019 | Universidad politécnica de Tlaxcala</p>
                             </div>
                             <p>CCNA Routing and Switching:Introduccion a las redes.</p>
                             <p>CCNA Routing and Switching:Principios basicos de routing y Switching.</p>
                             <p>CCNA Routing and Switching:Escalamiento de redes.</p>
                             <p>CCNA Routing and Switching:Connecting Networks.</p>
 
                         </div>
                    </div>
                </div>
            </div>
        
            <!-- Skills Row Start -->
            <div class="row mt-100">
            
                <!-- Header Block -->
                <div class="col-md-12">
                    <div class="header-box mb-50">
                        <h3>Habilidades</h3>
                    </div>
                </div>
            </div>
            
            <div class="box skills">
                    <h5>Lenguaje PHP</h5>
                    <p></p>
                <div class="row">
 
 
 
                    <div class="col-lg-12 col-sm-18">
                        <div class="row">
                            
                            <!-- Skill Item -->
                            <div class="col-lg-3 col-sm-6">
                                <div class="chart" data-percent="100" data-bar-color="#25ca7f"><p>Php</p></div>
                            </div>
                            
                            <!-- Skill Item -->
                            <div class="col-lg-3 col-sm-6">
                                <div class="chart" data-percent="100" data-bar-color="#25ca7f"><p>Laravel</p></div>
                            </div>
                            
                            <!-- Skill Item -->
                            <div class="col-lg-3 col-sm-6">
                                <div class="chart" data-percent="100" data-bar-color="#25ca7f"><p>Vue</p></div>
                            </div>
                            
                            <!-- Skill Item -->
                            <div class="col-lg-3 col-sm-6">
                                <div class="chart" data-percent="100" data-bar-color="#25ca7f"><p>Node.js</p></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
 
            <div class="row mt-50">
            
            </div>
 
 
 
            <div class="box skills">
                    <h5>Lenguaje Python</h5>
                    <p></p>
                <div class="row">
 
 
 
                    <div class="col-lg-12 col-sm-18">
                        <div class="row">
                            
                            <!-- Skill Item -->
                            <div class="col-lg-3 col-sm-6">
                                <div class="chart" data-percent="100" data-bar-color="#25ca7f"><p>Python</p></div>
                            </div>
                            
                            <!-- Skill Item -->
                            <div class="col-lg-3 col-sm-6">
                                <div class="chart" data-percent="100" data-bar-color="#25ca7f"><p>Django</p></div>
                            </div>
                            
                            <!-- Skill Item -->
                            <div class="col-lg-3 col-sm-6">
                                <div class="chart" data-percent="100" data-bar-color="#25ca7f"><p>Vue</p></div>
                            </div>
                            
                            <!-- Skill Item -->
                            <div class="col-lg-3 col-sm-6">
                                <div class="chart" data-percent="100" data-bar-color="#25ca7f"><p>item1</p></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
 
 
 
            <div class="row mt-50">
            
            </div>
 
 
 
            <div class="box skills">
                    <h5>Lenguaje Java</h5>
                    <p></p>
                <div class="row">
 
 
 
                    <div class="col-lg-12 col-sm-18">
                        <div class="row">
                            
                            <!-- Skill Item -->
                            <div class="col-lg-3 col-sm-6">
                                <div class="chart" data-percent="100" data-bar-color="#25ca7f"><p>Java</p></div>
                            </div>
                            
                            <!-- Skill Item -->
                            <div class="col-lg-3 col-sm-6">
                                <div class="chart" data-percent="100" data-bar-color="#25ca7f"><p>Spring</p></div>
                            </div>
                            
                            <!-- Skill Item -->
                            <div class="col-lg-3 col-sm-6">
                                <div class="chart" data-percent="100" data-bar-color="#25ca7f"><p>item</p></div>
                            </div>
                            
                            <!-- Skill Item -->
                            <div class="col-lg-3 col-sm-6">
                                <div class="chart" data-percent="100" data-bar-color="#25ca7f"><p>item1</p></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
 
 
 
 
            <div class="row mt-50">
            
            </div>
 
 
 
            <div class="box skills">
                    <h5>Complemento</h5>
                    <p></p>
                <div class="row">
 
 
 
                    <div class="col-lg-12 col-sm-18">
                        <div class="row">
                            
                            <!-- Skill Item -->
                            <div class="col-lg-3 col-sm-6">
                                <div class="chart" data-percent="100" data-bar-color="#25ca7f"><p>MVC</p></div>
                            </div>
                            
                            <!-- Skill Item -->
                            <div class="col-lg-3 col-sm-6">
                                <div class="chart" data-percent="100" data-bar-color="#25ca7f"><p>GitHub/ Bitbucket</p></div>
                            </div>
                            
                            <!-- Skill Item -->
                            <div class="col-lg-3 col-sm-6">
                                <div class="chart" data-percent="100" data-bar-color="#25ca7f"><p>Mysql/Sql server</p></div>
                            </div>
                            
                            <!-- Skill Item -->
                            <div class="col-lg-3 col-sm-6">
                                <div class="chart" data-percent="100" data-bar-color="#25ca7f"><p>Trello</p></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
 
 
 
 
 
 
 
 
 
 
 
            <!-- Work Process Row Start -->
            <div class="row mt-100">
            
                <!-- Header Block -->
                <div class="col-md-12">
                    <div class="header-box mb-50">
                        <h3>Mi proceso de trabajo</h3>
                    </div>
                </div>
            </div>
            
            <div class="box work-process mb-100">
                <div class="row">
                    <div class="col-lg-4 col-sm-12 ltr">
                    
                        <!-- Working Process Item-->
                           <div class="single-wp width-sm process-1">
                            <p class="wp-step">01</p>
                            <h4 class="wp-title">Definir la idea</h4>
                            <p>Definicíon de la idea principal del cliente para establecer los requerimientos.</p>
                        </div>
 
                        <!-- Working Process Item-->
                        <div class="single-wp width-sm process-2">
                            <p class="wp-step">02</p>
                            <h4 class="wp-title">Desarrollo</h4>
                            <p>Construir o desarrollar una solución mediante los requerimientos obtenidos por el cliente.</p>
                        </div>
                    </div>
            
                    <div class="col-lg-4 hidden-sm">
                    
                        <!-- Working Process Circle-->
                        <div class="wp-circle">
                            <h4>Proceso de trabajo</h4>
                            <span class="dots top-l"></span>
                            <span class="dots bottom-l"></span>
                            <span class="dots top-r"></span>
                            <span class="dots bottom-r"></span>
                        </div>
                    </div>
            
                    <div class="col-lg-4 col-sm-12 rtl">
                
                        <!-- Working Process Item-->
                        <div class="single-wp width-sm process-3">
                            <p class="wp-step">03</p>
                            <h4 class="wp-title">Pruebas</h4>
                            <p>Se implementan pruebas para prevenir y corregir algun problema en el desarrollo y asi poder implementar el proyecto de una forma optima.</p>
                        </div>
 
                        <!-- Working Process Item-->
                        <div class="single-wp width-sm process-4">
                            <p class="wp-step">04</p>
                            <h4 class="wp-title">Implementación e instalación</h4>
                            <p>Se instala o implementa el proyecto terminado.</p>
                        </div>
                    </div>
                </div>
            </div>
          </section>
     </div>


@endsection