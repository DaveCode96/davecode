@extends('layouts.plantilla')

@section('title', 'Dave Code')
<!-- para un contenido mas amplio -->
@section('content')

<div id="home">
<div id="about">
<div id="resume">
<div id="portfolio">
<div id="blog">
<div id="contact">
    
    <!-- Home Section -->
    <div class="pt-home" style="background-image: url('img/home.jpg')">
        <section>
           
           <!-- Banner -->
           <div class="banner">
                 <h1>David Alpizar. . .</h1>
               <p class="cd-headline rotate-1">
                   <span>I am a</span>
                   <span class="cd-words-wrapper">
                       <b class="is-visible">Developer</b>
                       <b>Freelancer</b>
                   </span>
               </p>
           </div>
           


           <!-- Social -->
           <div class="social">
               <ul>
                   <li><a href="https://www.facebook.com/Dav3Cod3"><i class="fab fa-facebook-f"></i></a></li>
                   <li><a href="https://twitter.com/DaveCode6"><i class="fab fa-twitter"></i></a></li>
                   <li><a href="https://www.instagram.com/davecode79/"><i class="fab fa-instagram"></i></a></li>
                   <li><a href="https://www.youtube.com/channel/UClqw9rBmhNdeFdh3IuuxyfQ"><i class="fab fa-youtube"></i></a></li>
               </ul>
           </div>
         </section>  
     </div>



 
    



{{-- 

   <!-- Portfolio Section -->
     <div class="page pt-portfolio" data-simplebar>
       <section class="container">
           
           <!-- Section Title -->
           <div class="header-page mt-70 mob-mt">
               <h2>Portafolio</h2>
               <span></span>
           </div>
           
           <!-- Portfolio Filter Row Start -->
           <div class="row mt-100">
               <div class="col-lg-12 col-sm-12 portfolio-filter">
                   <ul>
                       <li class="active" data-filter="*">All</li>
                       <li data-filter=".brand">Android</li>
                       <li data-filter=".design">Java</li>
                       <li data-filter=".graphic">PHP</li>
                   </ul>
               </div>
           </div>
           
           <!-- Portfolio Item Row Start -->
           <div class="row portfolio-items mt-100 mb-100">
           
               <!-- Portfolio Item -->
               <div class="item col-lg-4 col-sm-6 graphic">
                   <figure>
                       <img alt="" src="img/portfolio/img-1.jpg">
                       <figcaption>
                           <h3>Project Name</h3>
                           <a href="#">www.google.com</a>
                           <p>Graphic</p><i class="fas fa-image"></i>
                           <a class="image-link" href="img/portfolio/img-1.jpg"></a>
                       </figcaption>
                   </figure>
               </div>
           
               <!-- Portfolio Item -->
               <div class="item col-lg-4 col-sm-6 design">
                   <figure>
                       <img alt="" src="img/portfolio/img-2.jpg">
                           <figcaption>
                           <h3>Project Name</h3>
                           <a href="#">www.google.com</a>
                           <p>Design</p><i class="fas fa-image"></i>
                           <a class="image-link" href="img/portfolio/img-2.jpg"></a>
                       </figcaption>
                   </figure>
               </div>
           
               <!-- Portfolio Item -->
               <div class="item col-lg-4 col-sm-6 brand">
                   <figure>
                       <img alt="" src="img/portfolio/img-3.jpg">
                       <figcaption>
                           <h3>Project Name</h3>
                           <a href="#">www.google.com</a>
                           <p>Graphic</p><i class="fas fa-video"></i>
                           <a class="video-link" href="https://www.youtube.com/watch?v=k_okcNVZqqI"></a>
                       </figcaption>
                   </figure>
               </div>
           
               <!-- Portfolio Item -->
               <div class="item col-lg-4 col-sm-6 graphic">
                   <figure>
                       <img alt="" src="img/portfolio/img-4.jpg">
                       <figcaption>
                           <h3>Project Name</h3>
                           <a href="#">www.google.com</a>
                           <p>Design</p><i class="fas fa-image"></i>
                           <a class="image-link" href="img/portfolio/img-4.jpg"></a>
                       </figcaption>
                   </figure>
               </div>
           
               <!-- Portfolio Item -->
               <div class="item col-lg-4 col-sm-6 design">
                   <figure>
                       <img alt="" src="img/portfolio/img-5.jpg">
                       <figcaption>
                           <h3>Project Name</h3>
                           <a href="#">www.google.com</a>
                           <p>Design</p><i class="fas fa-video"></i>
                           <a class="video-link" href="https://www.youtube.com/watch?v=k_okcNVZqqI"></a>
                       </figcaption>
                   </figure>
               </div>
           
               <!-- Portfolio Item -->
               <div class="item col-lg-4 col-sm-6 brand">
                   <figure>
                       <img alt="" src="img/portfolio/img-6.jpg">
                       <figcaption>
                           <h3>Project Name</h3>
                           <a href="#">www.google.com</a>
                           <p>Brand</p><i class="fas fa-image"></i>
                           <a class="image-link" href="img/portfolio/img-6.jpg"></a>
                       </figcaption>
                   </figure>
               </div>
           
               <!-- Portfolio Item -->
               <div class="item col-lg-4 col-sm-6 graphic">
                   <figure>
                       <img alt="" src="img/portfolio/img-7.jpg">
                       <figcaption>
                           <h3>Project Name</h3>
                           <a href="#">www.google.com</a>
                           <p>Brand</p><i class="fas fa-image"></i>
                           <a class="image-link" href="img/portfolio/img-7.jpg"></a>
                       </figcaption>
                   </figure>
               </div>
           
               <!-- Portfolio Item -->
               <div class="item col-lg-4 col-sm-6 design">
                   <figure>
                       <img alt="" src="img/portfolio/img-8.jpg">
                       <figcaption>
                           <h3>Project Name</h3>
                           <a href="#">www.google.com</a>
                           <p>Brand</p><i class="fas fa-image"></i>
                           <a class="image-link" href="img/portfolio/img-8.jpg"></a>
                       </figcaption>
                   </figure>
               </div>
           
               <!-- Portfolio Item -->
               <div class="item col-lg-4 col-sm-6 brand">
                   <figure>
                       <img alt="" src="img/portfolio/img-9.jpg">
                       <figcaption>
                           <h3>Project Name</h3>
                           <a href="#">www.google.com</a>
                           <p>Graphic</p><i class="fas fa-image"></i>
                           <a class="image-link" href="img/portfolio/img-9.jpg"></a>
                       </figcaption>
                   </figure>
               </div>
           </div>
       </section>
         </div>





    <!-- Blog Section -->
     <div class="page pt-blog" data-simplebar>
       <section class="container">
           
           <!-- Section Title -->
            <div class="header-page mt-70 mob-mt">
               <h2>Blog</h2>
               <span></span>
           </div>
           
           <!-- Blog Row Start -->
           <div class="row blog-masonry mt-100 mb-50">
               
               <!-- Blog Item -->
               <div class="col-lg-4 col-sm-6">
                   <div class="blog-item">
                       <div class="thumbnail">
                           <a href="Blog/variable del articulo"><img alt="" src="img/blog/img-1.jpg"></a>
                       </div>
                       <h4><a href="Blog/variable del articulo">Titulo</a></h4>
                       <ul>
                           <li><a href="#">15 April 2019</a></li>
                           <li><a href="#">tema</a></li>
                          </ul>
                       <p>Tower Hamlets or mass or members of propaganda bananas real estate. However, a large and a mourning, vel euismod.</p>
                       <div class="blog-btn">
                           <a href="article.html" class="btn-st">Read More</a>
                       </div>
                   </div>
               </div>



               <!-- Blog Item -->
               <div class="col-lg-4 col-sm-6">
                   <div class="blog-item">
                       <div class="thumbnail">
                           <a href="article.html"><img alt="" src="img/blog/img-2.jpg"></a>
                           
                       </div>
                       <h4><a href="article.html">Titulo</a></h4>
                       <ul>
                           <li><a href="#">10 March 2019</a></li>
                           <li><a href="#">tema</a></li>
                          </ul>
                   <p>Tower Hamlets or mass or members of propaganda bananas real estate. However, a large and a mourning, vel euismod.</p>
                       <div class="blog-btn">
                           <a href="article.html" class="btn-st">Read More</a>
                       </div>
                   </div>
               </div>

               <!-- Blog Item -->
               <div class="col-lg-4 col-sm-6">
                   <div class="blog-item">
                       <div class="thumbnail">
                           <a href="article.html"><img alt="" src="img/blog/img-3.jpg"></a>
                       </div>
                       <h4><a href="article.html">Titulo</a></h4>
                       <ul>
                           <li><a href="#">02 March 2019</a></li>
                           <li><a href="#">Work</a></li>
                       </ul>
                       <p>Tower Hamlets or mass or members of propaganda bananas real estate. However, a large and a mourning, vel euismod.</p>
                       <div class="blog-btn">
                           <a href="article.html" class="btn-st">Read More</a>
                       </div>
                   </div>
               </div>

               <!-- Blog Item -->
               <div class="col-lg-4 col-sm-6">
                   <div class="blog-item">
                       <div class="thumbnail">
                           <a href="article.html"><img alt="" src="img/blog/img-4.jpg"></a>
                       </div>
                       <h4><a href="article.html">Titulo</a></h4>
                       <ul>
                           <li><a href="#">29 March 2019</a></li>
                           <li><a href="#">Career</a></li>
                       </ul>
                       <p>Tower Hamlets or mass or members of propaganda bananas real estate. However, a large and a mourning, vel euismod.</p>
                       <div class="blog-btn">
                           <a href="article.html" class="btn-st">Read More</a>
                       </div>
                   </div>
               </div>

               <!-- Blog Item -->
               <div class="col-lg-4 col-sm-6">
                   <div class="blog-item">
                       <div class="thumbnail">
                           <a href="article.html"><img alt="" src="img/blog/img-5.jpg"></a>
                       </div>
                       <h4><a href="article.html">Titulo</a></h4>
                       <ul>
                           <li><a href="#">14 April 2019</a></li>
                           <li><a href="#">tema</a></li>
                       </ul>
                       <p>Tower Hamlets or mass or members of propaganda bananas real estate. However, a large and a mourning, vel euismod.</p>
                       <div class="blog-btn">
                           <a href="article.html" class="btn-st">Read More</a>
                       </div>
                   </div>
               </div>

               <!-- Blog Item -->
               <div class="col-lg-4 col-sm-6">
                   <div class="blog-item">
                       <div class="thumbnail">
                           <a href="article.html"><img alt="" src="img/blog/img-6.jpg"></a>
                           <a href="https://www.youtube.com/watch?v=k_okcNVZqqI" class="btn-play"></a>
                       </div>
                       <h4><a href="article.html">Titulo</a></h4>
                       <ul>
                               <li><a href="#">29 April 2019</a></li>
                               <li><a href="#">Career</a></li>
                       </ul>
                       <p>Tower Hamlets or mass or members of propaganda bananas real estate. However, a large and a mourning, vel euismod.</p>
                       <div class="blog-btn">
                           <a href="article.html" class="btn-st">Read More</a>
                       </div>
                   </div>
               </div>
           </div>
       </section>
   </div>
 --}}
           
       </section>
     </div> 


     
</div>
</div>
</div>
</div>
</div>
</div>
@endsection
