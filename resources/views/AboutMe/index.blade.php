@extends ('layouts.plantilla')

@section('title', 'About Me')
    

@section('content')

<div class="pt-home" style="background-image: url('img/home.jpg')">
    <section>
    </section>
</div>
 <!-- About Section -->
 <div class="page pt-about" data-simplebar>
    <section class="container">
        
        <!-- Section Title -->
        <div class="header-page mt-70 mob-mt">
            <h2>Acerca de Mi</h2>
            <span></span>
        </div>
    
        <!-- Personal Info Start -->
        <div class="row mt-100">
        
            <!-- Information Block -->
            <div class="col-lg-12 col-sm-12">
                <div class="info box">
                    <div class="row">
                        <div class="col-lg-3 col-sm-4">
                            <div class="photo">
                                <img alt="" src="img/user-photo.jpg">		
                            </div>	
                        </div>
                        <div class="col-lg-9 col-sm-8">
                            <h4>Dave Code</h4>
                            <div class="loc">
                                <i class="fas fa-map-marked-alt"></i> Tlaxcala, Mexico
                            </div>
                            <p>Hola! Soy David Alpizar un desarrollador apasionado por la tecnología y el emprendimiento. Empeze con la tecnologia cuando tenia 11 años y actualmente es a lo que me dedico y me apasiona.
                            </p>
                        </div>
                        
                        <!-- Icon Info -->
                        <div class="col-lg-3 col-sm-4">
                            <div class="info-icon">
                                 <i class="fas fa-award"></i>
                                 <div class="desc-icon">
                                    <h6>1 año</h6>
                                    <p>Experiencia</p>
                                   </div>
                            </div>
                        </div>
                        
                        <!-- Icon Info -->
                        <div class="col-lg-3 col-sm-4">
                            <div class="info-icon">
                                 <i class="fas fa-certificate"></i>
                                 <div class="desc-icon">
                                    <h6>1 Proyecto</h6>
                                    <p>Completado</p>
                                  </div>
                            </div>
                        </div>
                        
                        <!-- Icon Info -->
                        <div class="col-lg-3 col-sm-4">
                            <div class="info-icon">
                                 <i class="fas fa-user-astronaut"></i>
                                 <div class="desc-icon">
                                    <h6>Freelance</h6>
                                       <p>Disponible</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-12 pt-50">
                            <a href="#" class="btn-st">Descargar CV</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Interests Row Start -->
        <div class="row mt-100">
        
            <!-- Header Block -->
            <div class="col-md-12">
                <div class="header-box mb-50">
                    <h3>Mis intereses</h3>
                </div>
            </div>
        
            <div class="col-lg-12 col-sm-12">
                <div class="box">
                    <div class="row">
                        <!-- Interests Item -->
                        <div class="col-lg-3 col-sm-6">
                            <div class="inter-icon">
                                 <i class="fas fa-music"></i>
                                 <div class="desc-inter">
                                    <h6>Musica</h6>
                                   </div>
                            </div>
                        </div>
                    
                        <!-- Interests Item -->
                        <div class="col-lg-3 col-sm-6">
                            <div class="inter-icon">
                                 <i class="fas fa-route"></i>
                                 <div class="desc-inter">
                                    <h6>Viajes</h6>
                                   </div>
                            </div>
                        </div>
                    
                        <!-- Interests Item -->
                        <div class="col-lg-3 col-sm-6">
                            <div class="inter-icon">
                                 <i class="fas fa-globe"></i>
                                     <div class="desc-inter">
                                    <h6>Tecnología</h6>
                                   </div>
                            </div>
                        </div>
                    
                        <!-- Interests Item -->
                        <div class="col-lg-3 col-sm-6">
                            <div class="inter-icon">
                                 <i class="fas fa-film"></i>
                                 <div class="desc-inter">
                                    <h6>Anime y Series</h6>
                                   </div>
                            </div>
                        </div>
                    
                        <!-- Interests Item -->
                        <div class="col-lg-3 col-sm-6">
                            <div class="inter-icon">
                                 <i class="fas fa-cogs"></i>
                                 <div class="desc-inter">
                                    <h6>Ajedrez</h6>
                                   </div>
                            </div>
                        </div>
                    
                        <!-- Interests Item -->
                        <div class="col-lg-3 col-sm-6">
                            <div class="inter-icon">
                                 <i class="fas fa-book"></i>
                                 <div class="desc-inter">
                                    <h6>Libros</h6>
                                   </div>
                            </div>
                        </div>
                    
                        <!-- Interests Item -->
                        <div class="col-lg-3 col-sm-6">
                            <div class="inter-icon">
                                 <i class="fas fa-gamepad"></i>
                                 <div class="desc-inter">
                                    <h6>Videojuegos</h6>
                                   </div>
                            </div>
                        </div>
                    
                        <!-- Interests Item -->
                        <div class="col-lg-3 col-sm-6">
                            <div class="inter-icon">
                                 <i class="fas fa-tree"></i>
                                 <div class="desc-inter">
                                    <h6>Naturaleza</h6>
                                   </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
        <!-- Service Row Start -->
        <div class="row mt-100">
        
            <!-- Header Block -->
            <div class="col-md-12">
                <div class="header-box mb-50">
                    <h3>Servicios</h3>
                </div>
            </div>
        
            <!-- Service Item -->
            <div class="col-lg-6 col-sm-6">
                <div class="service box mb-40">
                    <i class="fas fa-desktop"></i>
                    <h4>Desarrollo Web</h4>
                    <p>Desarrollo de sitios web a la medida para pequeñas empresas y paginas web personales para aquellos que quieren darse a conocer en internet.
                    </p>
                </div>
            </div>

            <!-- Service Item -->
            <div class="col-lg-6 col-sm-6">
                <div class="service box mb-40">
                    <i class="fas fa-cogs"></i>
                    <h4>Punto de Venta</h4>
                    <p>Implementación de punto de venta para control y optimización de las ventas en su negocio(Equipo de computo, escaner, caja de dinero, impresora de tickets).</p>
                </div>
            </div>

            <!-- Service Item -->
            <div class="col-lg-6 col-sm-6">
                <div class="service box mb-40">
                    <i class="fas fa-mobile-alt"></i>
                    <h4>Desarrollo Android</h4>
                    <p>Desarrollo de aplicaciones movíl para pequeñas empresas y proyectos personales.</p>
                </div>
            </div>

            <!-- Service Item -->
            <div class="col-lg-6 col-sm-6">
                <div class="service box mb-40">
                    <i class="fas fa-medkit"></i>
                    <h4>Soporte Técnico TI</h4>
                    <p>Mantenimiento preventivo y correctivo a equipos de computo para pequeños negocios.</p>

                </div>
            </div>
        </div>

      </section>
 </div>



@endsection